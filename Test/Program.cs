﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    using SQLiteLinq;
    using Test.SA1Model;

    class Program
    {
        static void Main(string[] args)
        {
            var db = new TBMDatabase();

            db.Initilize();
        }
    }    
}

namespace Test.SA1Model
{
    using SQLiteLinq;

    class TBMDatabase : Database
    {
        public override void Initilize()
        {
            CreateTable(from s in Station.Of(this)
                        from p in Project.Of(this)
                        from r in Reading.Of(this)
                        where s.NotNull(s.ID)
                        where s.ForeignKeyDefault(new { s.ID, s.Name }, new { p.ID, p.Name })
                        where s.ForeignKey(new { s.ID, s.Name }, new { p.ID, p.Name }).OnDelete(OnDeleteUpdateOption.CASCADE)
                        orderby s.ID descending, s.Name descending
                        select s);
        }
    }

    class Project : Table<Project>
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    class Station : Table<Station>
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int ProjID { get; set; }
    }

    class Reading : Table<Project>
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public int StationID { get; set; }
    }
}
