﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLiteLinq
{
    public static class DataTypes
    {
        public static Type[] LegitColumnTypes { get; set; }

        static DataTypes()
        {
            LegitColumnTypes = new Type[] { 
                typeof(int), 
                typeof(Int64), 
                typeof(double), 
                typeof(string), 
                typeof(DateTime), 
                typeof(byte[]) 
            };
        }
    }
}
