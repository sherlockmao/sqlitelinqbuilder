﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SQLiteLinq
{
    public partial class QueryContext<T> : IQueryProvider, IOrderedQueryable<T>
    {
        internal Database Database { get; set; }

        public IQueryProvider Provider { get { return this; } }

        public QueryContext(Database db, Expression exp)
        {
            Database = db;

            Expression = exp == null ? Expression.Constant(this) : exp;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ExecuteSQL<T>().GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new ArgumentException();
        }

        public Type ElementType
        {
            get { return typeof(T); }
        }

        public System.Linq.Expressions.Expression Expression { get; protected set; }

        public IQueryable<TElement> CreateQuery<TElement>(Expression expression)
        {
            return new QueryContext<TElement>(this.Database, expression);
        }

        public IQueryable CreateQuery(Expression expression)
        {
            return new QueryContext<T>(this.Database, expression);
        }

        public TResult Execute<TResult>(Expression expression)
        {
            var res = ExecuteSQL<TResult>();

            if (res != null && res.Count() > 0) return res.First();

            throw new QueryFailedException();
        }

        public object Execute(Expression expression)
        {
            throw new ArgumentException();
        }
    }
}
