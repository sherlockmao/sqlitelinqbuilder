﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace SQLiteLinq
{
    public partial class CreateTableVisitor<T> : ExpressionVisitor
    {
        internal Database DB { get; set; }
        internal TableInfo Tbl { get; set; }

        public CreateTableVisitor(Database db)
        {
            if (db == null) throw new ArgumentNullException();
            DB = db;

            FetchorCreateTableInfo();
        }

        private void FetchorCreateTableInfo()
        {
            var tableName = typeof(T).Name;

            if (DB.Tables.ContainsKey(tableName))
            {
                Tbl = DB.Tables[tableName];
            }
            else
            {
                Tbl = new TableInfo(tableName);
                DB.Tables.Add(tableName, Tbl);

                ExtractColumns();
            }
        }

        private void ExtractColumns()
        {
            var columns = typeof(T).GetProperties();

            if (columns == null && columns.Count() == 0)
                throw new ArgumentNullException("Table should not have 0 columns.");        

            foreach (var c in columns)
            {
                if (!DataTypes.LegitColumnTypes.Contains(c.PropertyType))
                {
                    throw new QueryFailedException("Columns only support int, double, string, and datetime.");
                }
                else
                {
                    var col = new ColumnInfo() { Name = c.Name, NUP = NotNullUniquePrimaryKey.NOT_NULL, Type = c.PropertyType };
                    Tbl[col.Name] = col;
                }
            }
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (!Visitors.ContainsKey(node.Method.Name))
                throw new QueryFailedException("Create Table should not contain invalid methods.");

            return Visitors[node.Method.Name].Visit(node);
        }

        protected Dictionary<string, ExpressionVisitor> Visitors =
            new Dictionary<string, ExpressionVisitor>()
        {
            {"Where", new WhereVisitor()},
            {"ThenBy", new OrderByVisitor()},
            {"OrderBy", new OrderByVisitor()},
            {"Select", null},
            {"SelectMany", null},
            {"ThenByDescending",null},
            {"OrderByDescending", null}
        };

    }
}
