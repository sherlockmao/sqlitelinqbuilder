﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SQLiteLinq
{
    public abstract class Table<T>
    {

        public static QueryContext<T> Of(Database db)
        {
            var neu = new QueryContext<T>(db, null);
            return neu;
        }

        public bool OnConflict(OnConflictOption opt) { return true; }
        public bool OnConflict<U>(U col, OnConflictOption opt) { return true; }

        public ForeignKeyonDeleteorUpdate ForeignKey<U>(U child, U parent) { return null; }
        public bool ForeignKeyDefault<U>(U child, U parent) { return true; }

        public bool Default<U>(U col, U val) { return true; }
        public bool Default<U>(U col, PresetDefaultValue val) { return true; }

        public bool Unique<U>(U col) { return true; }
        public bool NotNull<U>(U col) { return true; }
    }
}
