﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLiteLinq
{
    internal class TableInfo
    {
        internal string Name { get; set; }

        internal bool WithoutRowID { get; set; }

        internal string[] PrimaryKeys { get; set; }

        internal ForeignKeyInfo[] ForeignKeys { get; set; }

        internal Dictionary<string, ColumnInfo> Columns { get; set; }

        public TableInfo(string name)
        {
            Name = name;

            Columns = new Dictionary<string, ColumnInfo>();
        }

        public ColumnInfo this[string columnName]
        {
            get
            {
                return Columns[columnName];
            }
            set
            {
                Columns[columnName] = value;
            }
        }
    }

    internal class ForeignKeyInfo
    {
        internal TableInfo ChildTable { get; set; }

        internal TableInfo ParentTable { get; set; }

        internal string[] ChildColumns { get; set; }

        internal string[] ParentColumns { get; set; }
    }
}
