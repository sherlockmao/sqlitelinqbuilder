﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLiteLinq
{
    internal class ColumnInfo
    {
        internal string Name { get; set; }
        internal Type Type { get; set; }
        internal NotNullUniquePrimaryKey NUP { get; set; }
        internal ColumnInfo ForeignKey { get; set; }
    }

    internal enum NotNullUniquePrimaryKey
    {
        NOT_NULL,
        UNIQUE,
        PRIMARY_KEY
    }
}
