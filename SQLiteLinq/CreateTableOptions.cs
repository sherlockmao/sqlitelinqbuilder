﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLiteLinq
{
    public enum OnConflictOption
    {
        ROLLBACK,
        ABORT,
        FAIL,
        IGNORE,
        REPLACE
    }

    public enum OnDeleteorUpdate
    {
        DELETE,
        UPDATE
    }

    public enum OnDeleteUpdateOption
    {
        SET_NULL,
        SET_DEFAULT,
        CASCADE,
        RESTRICTED,
        NO_ACTION
    }

    public enum DeferredorImmediate
    {
        DEFERRABLE_INITIALLY_DEFERRED,
        DEFERRABLE_INITIALLY_IMMEDIATE
    }

    public enum PresetDefaultValue
    {
        CURRENT_TIME,
        CURRENT_DATE,
        CURRENT_TIMESTAMP,
        NULL
    }


    public class ForeignKeyonDeleteorUpdate
    {
        public ForeignKeyDeferredorImmediate OnDelete(OnDeleteUpdateOption opt) { return null; }
        public ForeignKeyDeferredorImmediate OnUpdate(OnDeleteUpdateOption opt) { return null; }

        public static implicit operator bool(ForeignKeyonDeleteorUpdate _) { return true; }
    }

    public class ForeignKeyDeferredorImmediate
    {
        public ForeignKeyonDeleteorUpdate Deferred() { return null; }
        public ForeignKeyonDeleteorUpdate Immediate() { return null; }

        public static implicit operator bool(ForeignKeyDeferredorImmediate _) { return true; }
    }

    internal static class ForeignKeyExtensions
    {
        internal static Dictionary<string, string> EnumwithSpace = new Dictionary<string, string>();

        internal static string ToStringUnderscoretoSpace<T>(this T self)
        {
            var literal = self.ToString();

            if (EnumwithSpace.ContainsKey(literal)) return EnumwithSpace[literal];

            var literalwithSpace = literal.Replace('_', ' ');

            EnumwithSpace[literal] = literalwithSpace;
            return literalwithSpace;
        }

    }
}
