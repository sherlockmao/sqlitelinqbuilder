﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLiteLinq
{
    public class QueryFailedException : Exception
    {
        public QueryFailedException(string msg = "Query failed") : base(msg) { }
    }
}
