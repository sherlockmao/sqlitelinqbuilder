﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SQLiteLinq
{
    public abstract class Database
    {
        public abstract void Initilize();
        public void CreateTable<T>(IQueryable<T> iqry)
        {
            var node = iqry.Expression;

            var visitor = new CreateTableVisitor<T>(this);

            visitor.Visit(node);

        }

        public Database()
        {
            Tables = new Dictionary<string, TableInfo>();
        }

        internal Dictionary<string, TableInfo> Tables { get; set; }
    }
}
